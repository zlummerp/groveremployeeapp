﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GroverEmployeeApp.Services
{
    public interface IDataStore<T>
    {
        Task<bool> AddEmployeeAsync(T employee);
        Task<bool> UpdateEmployeeAsync(T employee);
        Task<bool> DeleteEmployeeAsync(string id);
        Task<int> DeleteAllEmployeesAsync();
        Task<T> GetEmployeeAsync(string id);
        Task<IEnumerable<T>> GetEmployeesAsync(bool forceRefresh = false);
    }
}
