﻿using GroverEmployeeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GroverEmployeeApp.Services
{
    public class MockDataStore : IDataStore<Employee>
    {
        readonly List<Employee> items;

        public MockDataStore()
        {
            items = new List<Employee>()
            {
                new Employee { Id = Guid.NewGuid().ToString(), Name = "First item", JobTitle="This is an item description.", StartDate=DateTime.Now },
                new Employee { Id = Guid.NewGuid().ToString(), Name = "Second item", JobTitle="This is an item description.", StartDate=DateTime.Now },
                new Employee { Id = Guid.NewGuid().ToString(), Name = "Third item", JobTitle="This is an item description.", StartDate=DateTime.Now },
                new Employee { Id = Guid.NewGuid().ToString(), Name = "Fourth item", JobTitle="This is an item description.", StartDate=DateTime.Now },
                new Employee { Id = Guid.NewGuid().ToString(), Name = "Fifth item", JobTitle="This is an item description.", StartDate=DateTime.Now },
                new Employee { Id = Guid.NewGuid().ToString(), Name = "Sixth item", JobTitle="This is an item description.", StartDate=DateTime.Now }
            };
        }

        public async Task<bool> AddEmployeeAsync(Employee employee)
        {
            items.Add(employee);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateEmployeeAsync(Employee employee)
        {
            var oldItem = items.Where((Employee arg) => arg.Id == employee.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(employee);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteEmployeeAsync(string id)
        {
            var oldItem = items.Where((Employee arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Employee> GetEmployeeAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Employee>> GetEmployeesAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }

        public Task<int> DeleteAllEmployeesAsync()
        {
            throw new NotImplementedException();
        }
    }
}