﻿using GroverEmployeeApp.Models;
using NuGet.Common;
using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace GroverEmployeeApp.Services
{
    public class EmployeeDatabase : IDataStore<Employee>
    {
        static SQLiteAsyncConnection Database;

        public static readonly AsyncLazy<EmployeeDatabase> DatabaseInstance = new AsyncLazy<EmployeeDatabase>(async () =>
        {
            var databaseInstance = new EmployeeDatabase();
            CreateTableResult result = await Database.CreateTableAsync<Employee>();
            return databaseInstance;
        });

        public EmployeeDatabase()
        {
            Database = new SQLiteAsyncConnection(Constants.Database.DatabasePath, Constants.Database.Flags);
        }

        public async Task<bool> AddEmployeeAsync(Employee employee)
        {
            // TODO: Make return id
            return await Database.InsertAsync(employee) != 0;
        }

        public async Task<bool> UpdateEmployeeAsync(Employee employee)
        {
            // TODO: Make return id
            // TODO: Make sure this doesn't break on new items maybe?
            return await Database.UpdateAsync(employee) != 0;
        }

        public async Task<bool> DeleteEmployeeAsync(string id)
        {
            var employee = await GetEmployeeAsync(id);
            return await Database.DeleteAsync(employee) != 0;
        }

        public async Task<int> DeleteAllEmployeesAsync()
        {
            return await Database.DeleteAllAsync<Employee>();
        }

        public async Task<Employee> GetEmployeeAsync(string id)
        {
            return await Database.Table<Employee>().Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public Task<IEnumerable<Employee>> GetEmployeesAsync(bool forceRefresh = false)
        {
            var employeeList = Database.Table<Employee>().ToListAsync().Result;
            return Task.FromResult(employeeList.AsEnumerable());
        }
    }
}
