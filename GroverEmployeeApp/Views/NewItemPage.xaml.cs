﻿using GroverEmployeeApp.Models;
using GroverEmployeeApp.ViewModels;
using Xamarin.Forms;

namespace GroverEmployeeApp.Views
{
    public partial class NewItemPage : ContentPage
    {
        public Employee Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();
            BindingContext = new NewItemViewModel();
        }
    }
}