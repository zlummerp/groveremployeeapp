﻿using GroverEmployeeApp.Models;
using GroverEmployeeApp.ViewModels;
using Xamarin.Forms;

namespace GroverEmployeeApp.Views
{
    public partial class UpdateItemPage : ContentPage
    {
        public Employee Item { get; set; }

        public UpdateItemPage()
        {
            InitializeComponent();
            BindingContext = new UpdateItemViewModel();
        }
    }
}