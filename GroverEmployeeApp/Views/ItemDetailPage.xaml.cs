﻿using GroverEmployeeApp.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace GroverEmployeeApp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}