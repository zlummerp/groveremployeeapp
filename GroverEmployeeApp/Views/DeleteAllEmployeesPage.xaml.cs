﻿using GroverEmployeeApp.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace GroverEmployeeApp.Views
{
    public partial class DeleteAllEmployeesPage : ContentPage
    {
        public DeleteAllEmployeesPage()
        {
            InitializeComponent();
            BindingContext = new DeleteAllEmployeesViewModel();
        }
    }
}