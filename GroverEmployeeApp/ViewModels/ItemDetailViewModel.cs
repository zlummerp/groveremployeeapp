﻿using GroverEmployeeApp.Views;
using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace GroverEmployeeApp.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        private string itemId;
        private string name;
        private string jobTitle;
        private DateTime startDate;
        public Command DeleteItemCommand { get; }
        public Command UpdateItemCommand { get; }

        public string Id { get; set; }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string JobTitle
        {
            get => jobTitle;
            set => SetProperty(ref jobTitle, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetProperty(ref startDate, value);
        }

        public string ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                LoadItemId(value);
            }
        }

        public ItemDetailViewModel()
        {
            DeleteItemCommand = new Command(OnDeleteEmployee);
            UpdateItemCommand = new Command(OnUpdateEmployee);
        }

        private async void OnDeleteEmployee()
        {
            await Database.DeleteEmployeeAsync(Id);

            await Application.Current.MainPage.DisplayAlert("", "Employee successfully deleted.", "OK");
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnUpdateEmployee()
        {
            await Shell.Current.GoToAsync($"{nameof(UpdateItemPage)}?{nameof(UpdateItemViewModel.ItemId)}={Id}");
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                var employee = await Database.GetEmployeeAsync(itemId);
                Id = employee.Id;
                Name = employee.Name;
                JobTitle = employee.JobTitle;
                StartDate = employee.StartDate;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}
