﻿using GroverEmployeeApp.Models;
using System;
using Xamarin.Forms;

namespace GroverEmployeeApp.ViewModels
{
    public class NewItemViewModel : BaseViewModel
    {
        private string name;
        private string jobTitle;
        private DateTime startDate = DateTime.Now;

        public NewItemViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave()
        {
            return !string.IsNullOrWhiteSpace(name)
                && !string.IsNullOrWhiteSpace(jobTitle);
        }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string JobTitle
        {
            get => jobTitle;
            set => SetProperty(ref jobTitle, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetProperty(ref startDate, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            Employee newEmployee = new Employee()
            {
                Id = Guid.NewGuid().ToString(),
                Name = Name,
                JobTitle = JobTitle,
                StartDate = StartDate
            };

            await Database.AddEmployeeAsync(newEmployee);

            await Application.Current.MainPage.DisplayAlert("", "Employee successfully created.", "OK");

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
