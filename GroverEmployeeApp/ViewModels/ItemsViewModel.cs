﻿using GroverEmployeeApp.Models;
using GroverEmployeeApp.Services;
using GroverEmployeeApp.Views;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GroverEmployeeApp.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        private Employee _selectedItem;

        public ObservableCollection<Employee> Items { get; }
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command DeleteAllEmployeesCommand { get; }
        public Command<Employee> ItemTapped { get; }

        public ItemsViewModel()
        {
            Title = "Browse";
            Items = new ObservableCollection<Employee>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            ItemTapped = new Command<Employee>(OnItemSelected);

            AddItemCommand = new Command(OnAddItem);
            DeleteAllEmployeesCommand = new Command(OnDeleteAllEmployees);
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                EmployeeDatabase employeeDatabase = await EmployeeDatabase.DatabaseInstance;
                var items = await employeeDatabase.GetEmployeesAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public Employee SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }

        private async void OnAddItem(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewItemPage));
        }

        private async void OnDeleteAllEmployees()
        {
            await Shell.Current.GoToAsync(nameof(DeleteAllEmployeesPage));
        }

        async void OnItemSelected(Employee item)
        {
            if (item == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.ItemId)}={item.Id}");
        }
    }
}