﻿using GroverEmployeeApp.Models;
using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace GroverEmployeeApp.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class UpdateItemViewModel : BaseViewModel
    {
        private string itemId;
        private string name;
        private string jobTitle;
        private DateTime startDate = DateTime.Now;

        public UpdateItemViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave()
        {
            return !string.IsNullOrWhiteSpace(name)
                && !string.IsNullOrWhiteSpace(jobTitle);
        }

        public string ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                LoadItemId(value);
            }
        }

        public string Id { get; set; }

        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public string JobTitle
        {
            get => jobTitle;
            set => SetProperty(ref jobTitle, value);
        }

        public DateTime StartDate
        {
            get => startDate;
            set => SetProperty(ref startDate, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            Employee employee = new Employee()
            {
                Id = Id,
                Name = Name,
                JobTitle = JobTitle,
                StartDate = StartDate
            };

            await Database.UpdateEmployeeAsync(employee);

            await Application.Current.MainPage.DisplayAlert("", "Employee successfully updated.", "OK");

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                var employee = await Database.GetEmployeeAsync(itemId);
                Id = employee.Id;
                Name = employee.Name;
                JobTitle = employee.JobTitle;
                StartDate = employee.StartDate;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}
