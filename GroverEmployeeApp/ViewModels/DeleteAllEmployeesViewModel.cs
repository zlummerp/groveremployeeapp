﻿using Xamarin.Forms;

namespace GroverEmployeeApp.ViewModels
{
    public class DeleteAllEmployeesViewModel : BaseViewModel
    {
        public Command DeleteAllEmployeesCommand { get; }
        public Command CancelCommand { get; }

        public DeleteAllEmployeesViewModel()
        {
            DeleteAllEmployeesCommand = new Command(OnDeleteAllEmployees);
            CancelCommand = new Command(OnCancel);
        }

        private async void OnDeleteAllEmployees()
        {
            await Database.DeleteAllEmployeesAsync();
            await Application.Current.MainPage.DisplayAlert("", "Employees successfully deleted.", "OK");

            await Shell.Current.GoToAsync("..");
        }

        private async void OnCancel()
        {
            await Shell.Current.GoToAsync("..");
        }
    }
}
