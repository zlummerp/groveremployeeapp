﻿using GroverEmployeeApp.ViewModels;
using GroverEmployeeApp.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace GroverEmployeeApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
            Routing.RegisterRoute(nameof(UpdateItemPage), typeof(UpdateItemPage));
            Routing.RegisterRoute(nameof(DeleteAllEmployeesPage), typeof(DeleteAllEmployeesPage));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}
