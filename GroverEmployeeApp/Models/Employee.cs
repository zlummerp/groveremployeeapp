﻿using SQLite;
using System;

namespace GroverEmployeeApp.Models
{
    public class Employee
    {
        [PrimaryKey]
        public string Id { get; set; }

        public string Name { get; set; }

        public string JobTitle { get; set; }

        public DateTime StartDate { get; set; }
    }
}